# \---- EMAIL \----

1. Configuración --> Configuraciones Generales
2. Añadir **Seudónimo del dominio** (casamador.com)

3. Configuración --> Usuarios
4. Seleccionamos cada uno de los usuarios y en la ficha de Preferencias introducimos el **Seudónimo de la mensajería** (xxxx@casamador.com)

5. Configuramos los servidores. En este caso Gmail.


### Configuración email entrante
---

1. Configuración --> Email --> Servidores de correo entrante
2. Crear
3. Cubrir formulario. Imagen adjunta.

![image](./img/gmail-entrante.png)

Por defecto Odoo comprueba el email cada hora. Se puede cambiar en:*
	
Configuración --> Automatización --> Acciones planificadas --> Email Queue Manager
	
Por ejemplo, para comprobar cada cinco minutos:*

![image](./img/comprobacion-mail.png)


  
### Configuración email saliente
---

1. Configuración --> Email --> Servidores de correo saliente
2. Crear
3. Cubrir formulario. Imagen adjunta.

![image](./img/gmail-saliente.png)



# \---- GOOGLE CALENDAR \----
---

1. Conectar con la cuenta de Google e ir a <https://console.developers.google.com/project>
2. Pulsar en **Crear proyecto**

![image](./img/gdeveloper-console.png)

![image](./img/gdeveloper-console2.png)

3. En el menú de la izquierda seleccionamos el submenú **API** y buscamos **Calendar**

![image](./img/gdeveloper-console3.png)

4. Una vez dentro hacemos click en **Habilitar API**

![image](./img/gdeveloper-console4.png)

5. En el menú de la izquierda seleccionamos el submenú **Credenciales** y clickamos en **Crear ID de cliente nuevo**

![image](./img/gdeveloper-console5.png)

6. Seleccionamos **Aplicación web** y clickamos en **Configurar la pantalla de consentimiento**

![image](./img/gdeveloper-console6.png)

7. Cubrimos **Dirección de correo electrónico** y **Nombre de producto** y guardamos.

![image](./img/gdeveloper-console7.png)

8. Eliminamos **Orígenes de JavaScript autorizados** y añadimos nuestro dominio seguido de /google_account/authentication (ej: https://54.171.116.250/google_account/authentication) a **URIs de redireccionamiento autorizados** para crear la ID de cliente.

![image](./img/gdeveloper-console8.png)

7. Obtenermos el **ID del cliente de Google** y la **Palabra secreta del cliente de Google** para añadir en las configuraciones generales de Odoo.



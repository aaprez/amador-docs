# Módulos instalados

Módulos básicos:

+ account\_accountant
+ account\_cancel
+ sale
+ purchase
+ point\_of\_sale
+ contacts

Módulos [localización española][1]:

+ l10n\_es
+ l10n\_es\_account\_asset
+ l10n\_es\_account\_balance\_report
+ l10n\_es\_account\_invoice\_sequence
+ l10n\_es\_aeat
+ l10n\_es\_aeat\_mod340
+ l10n\_es\_fiscal\_year\_closing
+ l10n\_es\_partner

# Módulos pendientes de revisión


[1]:	https://github.com/OCA/l10n-spain
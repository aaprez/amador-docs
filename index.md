# Manual de uso de Odoo v8 adaptado a las necesidades de Casa Amador
---


### Índice de contenidos
+ Apuntes Técnicos
	- Instalación
		- [Módulos instalados](./tecnico/install.md#Módulos instalados)
		- [Módulos pendientes de revisión](./tecnico/install.md#Módulos pendientes de revisión)
+ Manual de uso
	- Básico
		- [Toma de contacto - Interfaz]()
